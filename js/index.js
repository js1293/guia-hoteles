$(function () {
    $('[data-toggle="tooltip"]').tooltip();
  })
  $('.carousel').carousel({
    interval: 1500
  })

  $('#bookingModal').on('show.bs.modal', function (e) {
    console.log("Se comienza a abrir la modal");
    $('.btnOpenModal').removeClass('btn-primary');
    $('.btnOpenModal').addClass('btn-outline-primary');
  })
  $('#bookingModal').on('shown.bs.modal', function (e) {
    console.log("Se abrió la modal");
  })
  $('#bookingModal').on('hide.bs.modal', function (e) {
    console.log("Se comienza a cerrar la modal");
    $('.btnOpenModal').removeClass('btn-outline-primary');
    $('.btnOpenModal').addClass('btn-primary');
  })
  $('#bookingModal').on('hidden.bs.modal', function (e) {
    console.log("Se cerró a abrir la modal");
  })